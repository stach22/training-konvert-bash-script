#!/usr/bin/env bash

# Copyright (c) 2017 Marek Stąsiek.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

rob() {
    file *txt | grep UTF > /dev/null
    if [ $? -ne 0 ]; then
        recode CP1250..UTF-8/CR-LF *txt
    fi
}

chodz() {
    pwd
    for katalog in *
    do
        if [ -d "$katalog" ]; then
            cd "$katalog"
            rob
            chodz "$katalog"
        fi
    done
    cd ..
}
cd "$1"
chodz "$1"